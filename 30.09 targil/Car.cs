﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageSpace
{
    class Car
    {
        public string PlateNumber { get; private set; } // 82-731-20
        public string Name { get; private set; }
        public float Price { get; private set; }
        public string Color { get; private set; }

        public Car(string plate_number, string name, float price, string color)
        {
            PlateNumber = plate_number;
            Name = name;
            Price = price;
            Color = color;
        }

        public override string ToString()
        {
            return $"Car No : {PlateNumber} Name : {Name} Price : {Price} Color : {Color} ";
        }
    }
}
