﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            Car honda = new Car("11-222-33", "honda civic", 80000, "black");
            Car mazda = new Car("11-765-33", "mazda lantis", 43000, "red");
            Car porche = new Car("11-899-33", "porche", 250000, "yellow");

            Garage gary_garage = new Garage();
            gary_garage.AddCar(honda);
            gary_garage.AddCar(mazda);
            gary_garage.AddCar(porche);

            Console.WriteLine($"print car with this plate number {gary_garage["11-899-33"]}");
            Console.WriteLine($"print car with this price {gary_garage[43000f]}");
            Console.WriteLine($"print car in the list in index 0 {gary_garage[0]}");
            Console.WriteLine();
            Console.WriteLine($"index 2 in garage: {gary_garage.GetCarByIndex(2)}");
            Console.WriteLine(gary_garage.GetCarByName("honda civic"));

            Car toyota = new Car("55-819-33", "toyota kamry", 130000, "blue");
            gary_garage[2] = toyota; // this will replace item number 2 to toyota
            Console.WriteLine($"index 2 in garage: {gary_garage.GetCarByIndex(2)}");

            Car fiat = new Car("85-819-35", "fiat 127", 25000, "white");
            gary_garage[3] = fiat; // if index is one above max then add this item to list , if it is higher, throw exception
            Console.WriteLine(gary_garage[3]);
        }


    }
}