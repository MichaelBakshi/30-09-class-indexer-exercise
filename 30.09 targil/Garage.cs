﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageSpace
{
    class Garage
    {
        private List<Car> m_cars = new List<Car>();

        public void AddCar(Car car)
        {
            if (car == null)
                throw new ArgumentException("cannot add <null> car to garage, dah?!");
            m_cars.Add(car);
        }

        public void AddLastCarToTheList(Car car)
        {
           
                    
        }   
        public Car this [string plateNumber]
        {
            get
            {
                Car result = m_cars.FirstOrDefault(car => car.PlateNumber == plateNumber);
                return result;
            }
        }

        public Car this[float price]
        {
            get
            {
                Car result = m_cars.FirstOrDefault(car => car.Price == price);
                return result;
            }
        }

        public Car this [int index]
        {
            get
            {
                if (index < 0 || index >= m_cars.Count)
                    return null;
                Car result = m_cars[index];
                return result;
            }
            internal set
            {
                if (index == m_cars.Count)
                {
                    AddCar(value);
                    return;
                }
                else if (index > m_cars.Count + 1)
                {
                    throw new IndexOutOfRangeException();
                }



                m_cars[index] = value;
                    //m_cars.AddCar(m_cars[index]);


            }
        }

        public Car GetCarByName (string name)
        {
            Car result = m_cars.FirstOrDefault(car => car.Name.ToUpper() == name.ToUpper());
            return result;
        }

        public Car GetCarByIndex (int index)
        {
            
            Car result = m_cars[index];
            return result;
        }

    }
}
